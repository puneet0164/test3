 provider "google" {
  version     = "= 3.34.0" 
  project     = "demoproject"
  region      = var.cluster_region
  credentials = file(var.access_key)
}


resource "google_container_cluster" "puneet-demo" {
  name                     = "puneet-demoo1"
  location                 = var.cluster_region
  min_master_version       = "1.15.12-gke.20"
  node_version             = "1.15.12-gke.20"
  network                  = "puneet-vpc" 
  
  subnetwork               = "puneet-subnet"

  private_cluster_config{
    enable_private_endpoint = true
    enable_private_nodes = true
    master_ipv4_cidr_block = "192.168.3.0/28"
  }

  ip_allocation_policy { }
  master_authorized_networks_config {
    cidr_blocks {
        cidr_block = "10.0.0.0/8" 
        display_name = "vpc_internal_connection"
    }
  }

  node_pool {
     name               = "kubernetes-pool"
     initial_node_count = "1"
     management {
        auto_repair  = true
        auto_upgrade = false
     }

     node_config {
         machine_type = "e2-small"
         disk_size_gb = "10"
         oauth_scopes = [
          "https://www.googleapis.com/auth/compute",
          "https://www.googleapis.com/auth/devstorage.read_only",
          "https://www.googleapis.com/auth/logging.write",
          "https://www.googleapis.com/auth/monitoring",
         ]
     }

  }
}


resource "null_resource" "delay" {
  depends_on = [google_container_cluster.puneet-demo]
    provisioner "local-exec" {
      command = "sleep 30"
    }
}


data "google_client_config" "default" {
    depends_on = [google_container_cluster.puneet-demo, null_resource.delay]
 }

data "google_container_cluster" "puneet-demo_config" {
    depends_on = [google_container_cluster.puneet-demo, null_resource.delay]
    name = var.puneet-demoo1
    zone = var.var.cluster_region
}
output "configvalue" {
  value = data.google_container_cluster.puneet-demo_config
}